Augmented Reality Tower Game Defense by Remi Cambuzat (2015)

Augmented Reality and Tangible Interaction Course - Université Paris-Sud - M2R Interaction 
(Jeam Marc Vezien  & Stéphane Huot)

This small project is a small AR tower defensse made using the Vuforia Unity Extension, and TDTK (TowerDefense ToolKit) for the game logic.

Link :

        Vuforia :
 https://developer.vuforia.com/downloads/sdk

        Tower Defense Toolkit (TDTK) Free : 
https://www.assetstore.unity3d.com/en/#!/content/5106

        3D Model From :
 https://www.assetstore.unity3d.com/en/#!/content/13295

Setup :

        Image Targets in Unity (Vuforia video tutorial) : 
https://developer.vuforia.com/library//articles/Training/Image-Targets-in-Unity


Credits photo :

(icon) http://reichshoffen.free.fr/Comple/reichshoffenBlason.jpg